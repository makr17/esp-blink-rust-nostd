# ESP Blink Rust no_std

Starting to play with embedded rust on the ESP32.
Eventual plan is to rebuild my
[Wifi Gate Controller](https://gitlab.com/makr17/wifi-gate-controller)
in rust.

But for now, _blinky_, the next step after literal "hello world".

To flash
```
cargo espflash --release /dev/ttyWHATEVER
```
Release build currently weighs in at 69KiB, debug at 4.0MiB.
Since the board only _has_ 4MiB of flash,
you almost certainly want to build/flash a release build.

